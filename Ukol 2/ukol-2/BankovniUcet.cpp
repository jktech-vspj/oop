#include "BankovniUcet.hpp"

int Length_Str(char *s)
{
	int n = 0;
	while (s[n] != '\0')
		n++;
	return n;
}

// ---

BankovniUcet::BankovniUcet(char *cislo_uctu) : BankovniUcet(cislo_uctu, MAX_VYBER)
{
	this->n_vklad = 0;
	this->n_vyber = 0;

	this->suma_vklad = 0;
	this->suma_vyber = 0;

	this->zustatek = 0;

	this->cislo_uctu = new char[strlen(cislo_uctu) + 1];
	strcpy_s(this->cislo_uctu, strlen(cislo_uctu) + 1,cislo_uctu);
	this->max_vyber = MAX_VYBER;
}

BankovniUcet::BankovniUcet(char *cislo_uctu, int max_vyber)
{
	this->n_vklad = 0;
	this->n_vyber = 0;

	this->suma_vklad = 0;
	this->suma_vyber = 0;

	this->zustatek = 0;

	this->cislo_uctu = new char[strlen(cislo_uctu) + 1];
	strcpy_s(this->cislo_uctu, strlen(cislo_uctu) + 1, cislo_uctu);
	this->max_vyber = max_vyber;
}

BankovniUcet::~BankovniUcet()
{
	delete[] this->cislo_uctu;
}

// ----

double BankovniUcet::GetZustatek() const
{
	return this->zustatek;
}

char* BankovniUcet::GetCisloUctu() const
{
	return this->cislo_uctu;
}

int BankovniUcet::GetMaxVyber() const
{
	return this->max_vyber;
}

void BankovniUcet::SetMaxVyber(int max_vyber)
{
	this->max_vyber = max_vyber;
}

int BankovniUcet::Get_n_vklad() const
{
	return this->n_vklad;
}

int BankovniUcet::Get_n_vyber() const
{
	return this->n_vyber;
}

double BankovniUcet::Get_suma_vklad() const
{
	return this->suma_vklad;
}

double BankovniUcet::Get_suma_vyber() const
{
	return this->suma_vyber;
}

// ----

bool BankovniUcet::Vlozit(double kolik)
{
	if (kolik >= 0) {
		this->n_vklad++;
		this->zustatek += kolik;
		this->suma_vklad += kolik;
		return true;
	}

	return false;
}

bool BankovniUcet::Vybrat(double kolik)
{
	if (kolik >= 0) {
		if (kolik <= this->zustatek) {
			if (this->max_vyber >= kolik) {
				this->n_vyber++;
				this->zustatek -= kolik;
				this->suma_vyber += kolik;
				return true;
			}
		}
	}

	return false;
}

// ----

/*friend*/ ostream& operator<<(ostream &os, const BankovniUcet &u)
{
	return os << u.GetCisloUctu() << " : " << u.GetZustatek() << " K�" << "\n" << u.Get_n_vklad() << " : +" << u.Get_suma_vklad() << " K�" << "\n" << u.Get_n_vyber() << " : +" << u.Get_suma_vyber() << " K�";
}
