#include "tiny-05.hpp"

namespace pjc {
    complex::complex(double real, double imaginary) :
        m_real(real),
        m_imag(imaginary) {}

    double complex::real() const {
        return m_real;
    }

    void complex::real(double d) {
        m_real = d;
    }

    double complex::imag() const {
        return m_imag;
    }

    void complex::imag(double d) {
        m_imag = d;
    }

    complex complex::operator+(complex const& rhs) {
        return complex(rhs.real() + m_real, rhs.imag() + m_imag);
    }

    complex complex::operator-(complex const& rhs) {
        return complex(m_real - rhs.real(), m_imag - rhs.imag());
    }

    complex complex::operator*(complex const& rhs) {
        double real = (rhs.real() * m_real) - (rhs.imag() * m_imag);
        double imag = (rhs.real() * m_imag) + (rhs.imag() * m_real);

        return complex(real, imag);
    }

    complex complex::operator+(double const& rhs) {
        return complex(m_real + rhs, m_imag);
    }

    complex complex::operator-(double const& rhs) {
        return complex(m_real - rhs, m_imag);
    }

    complex complex::operator*(double const& rhs) {
        double real = (rhs * m_real);
        double imag = (rhs * m_imag);

        return complex(real, imag);
    }

    complex pjc::operator+(double const& lhs, complex const& rhs) {
        return complex(rhs.real() + lhs, rhs.imag());
    }

    complex pjc::operator-(double const& lhs, complex const& rhs) {
        //Imagin�rn� ��st n�sob�m -1, proto�e imagin�rn� ��st pro lhs je v�dy 0, tak�e v�sledn� hodnota je v�dy p�evr�cen� imagin�rn� hodnota rhs
        return complex(lhs - rhs.real(), rhs.imag() * (-1));
    }

    complex pjc::operator*(double const& lhs, complex const& rhs) {
        double real = (rhs.real() * lhs);
        double imag = (rhs.imag() * lhs);

        return complex(real, imag);
    }
}